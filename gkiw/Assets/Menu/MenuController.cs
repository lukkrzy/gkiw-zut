﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject menu = null;
    public GameObject info = null;
    public GameObject levels = null;

    public void StartGame(int level)
    {
        //Debug.Log("lEVEL VAR: " + level);
        //Debug.Log("cHANGE TO SCENE: " + (level - 1));
        SceneManager.LoadScene(level-1);
        menu.SetActive(false);

    }



    public void Info()
    {
        info.SetActive(true);
        menu.SetActive(false);
    }

    public void Levels()
    {
        levels.SetActive(true);
        menu.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }


    public void Back()
    {
        menu.SetActive(true);
        info.SetActive(false);
        levels.SetActive(false);
    }


}