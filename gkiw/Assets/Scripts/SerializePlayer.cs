﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Text;

public class SerializePlayer : MonoBehaviour
{
    GameObject player = null;
    public GameObject respawn = null;
    // Use this for initialization
    void Start()
    {
         player = GameObject.Find("CharacterAnimation");

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {

            SaveGame();

        }
        else if (Input.GetKeyDown(KeyCode.F6))
        {
            LoadGame();
        }
    }

    void SaveGame()
    {
        string fileName = "data.xml";
        // Utworzenie obiektu odpowiedzialnego za serializację
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerDetails));
        // Utworzenie obiektów podlegających serializacji
        PlayerDetails pd = new PlayerDetails();
        pd.id = 1;
        pd.pos = player.transform.position;
        pd.itemId = player.GetComponent<ItemCollector>().getItem().name;       

        using (StreamWriter stream = new StreamWriter(fileName, false, Encoding.UTF8))
        {
            // Setrializacja obiektu klasy PersonalData
            serializer.Serialize(stream, pd);
        }

        Debug.Log("Game saved!");
    }

    void LoadGame()
    {
        string fileName = "data.xml";
        // Sprawdzenie czy plik istnieje
        if (File.Exists(fileName))
        {
            // Utworzenie obiektu odpowiedzialnego za deserializację
            XmlSerializer serializer = new XmlSerializer(typeof(PlayerDetails));
            PlayerDetails pd;
            // StreamReader tworzy strumień do odczytu
            using (StreamReader stream = new StreamReader(fileName))
            {
                // Deserialiacja obiektu klasy PersonalData
                pd = serializer.Deserialize(stream) as PlayerDetails;
            }

            player.transform.position = respawn.GetComponent<RespawnPoints>().GetClosestRespawnPoint(pd.pos);

            if(player.GetComponent<ItemCollector>().isItem())
            {
                Debug.Log("is item!");
                player.GetComponent<ItemCollector>().DropItem();
                player.GetComponent<ItemCollector>().CollectItemFromFile(GameObject.Find(pd.itemId));
                player.GetComponent<ItemCollector>().UpdateSprite();
            }
            Debug.Log("Game loaded!");
        }
    }
}
