﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImage : MonoBehaviour
{
    // Obiekt Image
    private Image image;
    public Sprite[] spriteArray;
    // Metoda zmieniająca bitmapę
    public void ChangeSprite(GameObject item)
    {
        if (item)
        {
            for (int i = 0; i < spriteArray.Length; ++i)
            {
               // Debug.Log("spriteArray[i].name: " + spriteArray[i].name);
               // Debug.Log("item.name: " + item.name);
                if (spriteArray[i].name == item.name)
                {
                    image.sprite = spriteArray[i];
                }
            }
        }
        else
        {
            image.sprite=spriteArray[0];
        }
    }
    void Start()
    {
        // Pobranie wskaźnika na komponent Image
        image = GetComponent<Image>();
        image.sprite = spriteArray[0];
    }
}