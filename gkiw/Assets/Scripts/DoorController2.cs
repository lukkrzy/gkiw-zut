﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController2 : MonoBehaviour
{

    public GameObject door = null;
    private Vector3 axis;
    public float rotateStep = 1.3F;
    private float currentAngle = 0.0F;
    private float maxAngle = 90.0F;
    private float minAngle = 0.0F;
    private bool rotate = false;
    private bool open = false;
    private bool isRotating = false;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        axis = door.transform.position + new Vector3(0, 0, -1.5F);
        audioSource = GetComponent<AudioSource>();


    }
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player" && !isRotating)
        {
                    rotate = true;
                    open = true;
                    isRotating = true;
                    audioSource.Play();
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" && !isRotating)
        {
            rotate = true;
            open = false;
            isRotating = true;
            audioSource.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {


        if (rotate && open)
        {
            if (currentAngle <= maxAngle)
            {
                door.transform.RotateAround(axis, Vector3.up, rotateStep);
                currentAngle += rotateStep;
            }
            else
            {
                rotate = false;
                isRotating = false;
            }
        }


        if (rotate && !open)
        {
            if (currentAngle >= minAngle)
            {
                door.transform.RotateAround(axis, Vector3.up, -rotateStep);
                currentAngle -= rotateStep;
            }
            else
            {
                rotate = false;
                isRotating = false;
            }
        }
    }
}
