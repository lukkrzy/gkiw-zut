﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Opponent")
        {
            collision.gameObject.GetComponent<OpponentHP>().health -= 50.0F;
        }

        if (collision.gameObject.name == "Opponent2")
        {
            collision.gameObject.GetComponent<OpponentHP2>().health -= 50.0F;
        }


        Destroy(gameObject);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}