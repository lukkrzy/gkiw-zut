﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPoints : Path
{

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < points.Length; ++i)
        {
            //path.Add (Vector2.zero);
            base.pathList.Add(points[i].transform.position);
        }
    }

    // Update is called once per frame
    protected void Update()
    {
        base.Update();
    }


    public GameObject[] points;
}