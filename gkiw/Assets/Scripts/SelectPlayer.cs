﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPlayer : MonoBehaviour {

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;

    public GameObject camera1;
    public GameObject camera2;
    public GameObject camera3;

    AudioListener camera1AudioLis;
    AudioListener camera2AudioLis;
    AudioListener camera3AudioLis;

    // Use this for initialization
    void Start()
    {
        player1.SetActive(false);
        player2.SetActive(false);
        player3.SetActive(true);

        camera1AudioLis = camera1.GetComponent<AudioListener>();
        camera2AudioLis = camera2.GetComponent<AudioListener>();
        camera3AudioLis = camera3.GetComponent<AudioListener>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            player1.SetActive(true);
            player2.SetActive(false);
            player3.SetActive(false);

            camera1.SetActive(true);
            camera1AudioLis.enabled = true;

            camera2AudioLis.enabled = false;
            camera2.SetActive(false);

            camera3AudioLis.enabled = false;
            camera3.SetActive(false);

            player1.GetComponent<ItemCollector>().UpdateSprite();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            player1.SetActive(false);
            player2.SetActive(true);
            player3.SetActive(false);

            camera1.SetActive(false);
            camera1AudioLis.enabled = false;

            camera2AudioLis.enabled = true;
            camera2.SetActive(true);

            camera3AudioLis.enabled = false;
            camera3.SetActive(false);

            player2.GetComponent<ItemCollector>().UpdateSprite();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            player1.SetActive(false);
            player2.SetActive(false);
            player3.SetActive(true);

            camera1.SetActive(false);
            camera1AudioLis.enabled = false;

            camera2AudioLis.enabled = false;
            camera2.SetActive(false);

            camera3AudioLis.enabled = true;
            camera3.SetActive(true);

            player3.GetComponent<ItemCollector>().UpdateSprite();
        }

    }


    
}