﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opponent : MonoBehaviour
{

    private Rigidbody rigidBody;
    private GameObject character;

    public float followDistance = 20.0F;
    public float slowDownDistance = 7.0F;
    public float stopDistance = 5.0F;
    public float rotationSpeed = 50.0F;
    public float minAngle = 10.0F;
    public float maxSpeed = 20.0F;
    // Use this for initialization
    void Start()
    {

        rigidBody = GetComponent<Rigidbody>();
        character = GameObject.Find("CharacterAnimation");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            character = GameObject.Find("Character");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            character = GameObject.Find("Character2");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            character = GameObject.Find("CharacterAnimation");
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

       // Debug.Log(Vector3.Distance(transform.position, character.transform.position));

        if (Vector3.Distance(transform.position, character.transform.position) < followDistance)
        {
            Vector3 characterDir = Vector3.RotateTowards(transform.forward, character.transform.position - transform.position, rotationSpeed * Time.deltaTime, 0.0F);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(characterDir), 0.1F);

            if (Vector3.Angle(characterDir, transform.forward) <= minAngle)
            {
                // Debug.Log("DISTANCE: " + Vector3.Distance(transform.position, character.transform.position));
                if (Vector3.Distance(transform.position, character.transform.position) >= slowDownDistance)
                {

                    if (rigidBody.velocity.magnitude <= maxSpeed)
                    {
                        characterDir *= maxSpeed;
                        rigidBody.AddForce(characterDir);
                //        Debug.Log("Velocity: " + rigidBody.velocity);
                    }
                    else
                    {
                        rigidBody.velocity = rigidBody.velocity.normalized * maxSpeed;
                    }
                }
                else
                {
                    if (Vector3.Distance(transform.position, character.transform.position) <= slowDownDistance && Vector3.Distance(transform.position, character.transform.position) >= stopDistance)
                    {
                        rigidBody.velocity = rigidBody.velocity * 0.9F;
                    }

                    else
                    {
                        rigidBody.velocity = Vector3.zero;
                    }
                }
            }

        }


    }
}
