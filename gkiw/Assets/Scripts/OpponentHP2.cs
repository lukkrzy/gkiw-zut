﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentHP2 : MonoBehaviour
{
    //public GameObject endGameScreen = null;
    public float health = 100.0F;
    public GameObject respawn = null;
    // Use this for initialization
    void Start()
    {
        health = 100.0F;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            //Destroy(gameObject);
            //endGameScreen.SetActive(true);
            transform.position = respawn.GetComponent<RespawnPoints>().GetRespawnPoint();
            health = 100.0F;
        }
    }


}