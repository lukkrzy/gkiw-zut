﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsRandom : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] Clips;

    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {

    }
    private void Step()
    {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
    }

    private AudioClip GetRandomClip()
    {

        return Clips[UnityEngine.Random.Range(0, Clips.Length)];
    }
}
