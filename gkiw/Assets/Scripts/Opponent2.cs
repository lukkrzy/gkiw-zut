﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opponent2 : MonoBehaviour
{

    private Rigidbody rigidBody;
    private GameObject character = null;

    public float followDistance = 20.0F;
    public float slowDownDistance = 7.0F;
    public float stopDistance = 5.0F;
    public float rotationSpeed = 50.0F;
    public float minAngle = 10.0F;
    public float maxSpeed = 20.0F;
    // Use this for initialization
    void Start()
    {

        rigidBody = GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider collider)
    {
       // Debug.Log("Enemy: Object entered the trigger: " + collider.gameObject.name);
        if (collider.gameObject.name == "CharacterAnimation")
            character = collider.gameObject;
        if (collider.gameObject.name == "Character")
            character = collider.gameObject;
        if (collider.gameObject.name == "Character2")
            character = collider.gameObject;

      //  Debug.Log("player in range: " + character.transform.position);

    }

    void OnTriggerStay(Collider collider)
    {

    }

    void OnTriggerExit(Collider collider)
    {
       // Debug.Log("Object exited the trigger: " + collider.gameObject.name);
        if (collider.gameObject == character)
            character = null;
    }



    // Update is called once per frame
    void FixedUpdate()
    {

        // Debug.Log(Vector3.Distance(transform.position, character.transform.position));
        if (character)
        {
            if (Vector3.Distance(transform.position, character.transform.position) < followDistance)
            {
                Vector3 characterDir = Vector3.RotateTowards(transform.forward, character.transform.position - transform.position, rotationSpeed * Time.deltaTime, 0.0F);

                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(characterDir), 0.1F);

                if (Vector3.Angle(characterDir, transform.forward) <= minAngle)
                {
                    // Debug.Log("DISTANCE: " + Vector3.Distance(transform.position, character.transform.position));
                    if (Vector3.Distance(transform.position, character.transform.position) >= slowDownDistance)
                    {

                        if (rigidBody.velocity.magnitude <= maxSpeed)
                        {
                            characterDir *= maxSpeed;
                            rigidBody.AddForce(characterDir);
                            //        Debug.Log("Velocity: " + rigidBody.velocity);
                        }
                        else
                        {
                            rigidBody.velocity = rigidBody.velocity.normalized * maxSpeed;
                        }
                    }
                    else
                    {
                        if (Vector3.Distance(transform.position, character.transform.position) <= slowDownDistance && Vector3.Distance(transform.position, character.transform.position) >= stopDistance)
                        {
                            rigidBody.velocity = rigidBody.velocity * 0.9F;
                        }

                        else
                        {
                            rigidBody.velocity = Vector3.zero;
                        }
                    }
                }

            }

        }
    }
}
