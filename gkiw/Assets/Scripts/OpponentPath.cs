﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentPath : MonoBehaviour
{

    private CharacterController controller;
    private Animator animator;
    public Path path;
    public bool loop = true;

    private int step = 0;

    public float stopDistance = 2.0F;
    public float rotationSpeed = 50.0F;
    public float minAngle = 20.0F;
    public float speed = 3.0F;
    // Use this for initialization
    void Start()
    {

        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 pathDir = path.pathList[step] - transform.position;
        //Debug.Log("Distance: "+Vector3.Distance(transform.position, pathDir));
        
        

        if (Vector3.Distance(transform.position, path.pathList[step]) > stopDistance)
        {
            Vector3 characterDir = Vector3.RotateTowards(transform.forward, path.pathList[step] - transform.position, rotationSpeed * Time.deltaTime, 0.0F);
            characterDir.y = 0;

            //Debug.Log("Angle: " + Vector3.Angle(characterDir, transform.forward));
            if (Vector3.Angle(characterDir, transform.forward) <= minAngle)
            {

                controller.Move(characterDir * speed * Time.deltaTime);
                animator.SetBool("Walk", true);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(characterDir), 0.05F);

            }
            else
            {
                animator.SetBool("Walk", false);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(characterDir), 0.1F);
            }
        }
        else
        {
            if(step< path.pathList.Count-1)
            step++;
            else
            {
                if (loop)
                    step = 0;
            }
        }


    }
}
