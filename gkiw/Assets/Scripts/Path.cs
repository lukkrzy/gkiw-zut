﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour {
    public List<Vector3> pathList = new List<Vector3>();
    public bool print = true;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	protected void Update () {
        if (print)
        {
            for (int i = 0; i < pathList.Count; i++)
            {
                Vector3 point = pathList[i];
                point.y += 5.0F;
                Debug.DrawLine(pathList[i], point, Color.red);
                //Debug.Log(pathList[i]+" to "+ point);

            }
        }
	}
    
}
