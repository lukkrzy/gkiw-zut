﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterWithAnimation : MonoBehaviour
{
    public float speed = 3.0f;
    public float rotateSpeed = 5.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private Animator animator;
    private int jumps;

    private GameObject item = null;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {

        if (controller.isGrounded)
        {
            //Debug.Log("is grounded");

            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            animator.SetFloat("MoveSpeed", Input.GetAxis("Vertical"));
            if (Input.GetButton("Fire1"))
            {
               // Debug.Log("is ctrl hold");
                animator.SetBool("Walk", true);
                moveDirection /= 2;

            } else
            {
                animator.SetBool("Walk", false);
            }
           // Debug.Log("Speed: " + Input.GetAxis("Vertical"));
            transform.Rotate(0, Input.GetAxis("Horizontal"), 0);

            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpSpeed;
                animator.SetBool("Jump", true);
            }
            else
            {
                animator.SetBool("Jump", false);
            }


        }

        moveDirection.y -= gravity * Time.deltaTime;

        controller.Move(moveDirection * Time.deltaTime);
    }
}
