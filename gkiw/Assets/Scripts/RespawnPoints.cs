﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPoints : MonoBehaviour {

    public GameObject[] points;
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public Vector3 GetRespawnPoint()
    {
        return points[Random.Range(0, points.Length)].transform.position;
    }
    public Vector3 GetClosestRespawnPoint(Vector3 playerPos)
    {
        float distance = 99999.0F;
        int closestRespawn = -1;
        for(int i=0;i<points.Length;i++)
        {
            float curDist = Vector3.Distance(playerPos, points[i].transform.position);
            if (curDist < distance)
            {
                distance = curDist;
                closestRespawn = i;
            }
        }
                
        return points[closestRespawn].transform.position;
    }
}
