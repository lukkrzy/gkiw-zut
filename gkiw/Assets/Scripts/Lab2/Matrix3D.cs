﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix3D
{
    private float[] matrix = {0, 0, 0, 0,
                                0, 0, 0, 0,
                                0, 0, 0, 0,
                                0, 0, 0, 0};

    public float this[int i]
    {
        get
        {
            if (i >= 0 && i < 16)
                return matrix[i];
            return 0;
        }

        set
        {
            if (i >= 0 && i < 16)
                matrix[i] = value;
        }

    }

    static public Matrix3D operator *(Matrix3D a, Matrix3D b)
    {
        Matrix3D result = new Matrix3D();
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                for (int k = 0; k < 4; ++k)
                {
                    result[4 * i + j] += a[i * 4 + k] * b[k * 4 + j];
                }
            }
        }

        return result;
    }

    static public Vector4 operator *(Matrix3D a, Vector4 v)
    {
        Vector4 result = new Vector4();

        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j)
                result[i] += a[i * 4 + j] * v[j];

        return result;
    }

    static public Matrix3D Translate(float x, float y, float z)
    {
        Matrix3D result = new Matrix3D();
        result[0] = 1;
        result[3] = x;
        result[5] = 1;
        result[7] = y;
        result[10] = 1;
        result[11] = z;
        result[15] = 1;
        return result;
    }

    static public Matrix3D Scale(float scaleX, float scaleY, float scaleZ)
    {
        Matrix3D result = new Matrix3D();
        result[0] = scaleX;
        result[5] = scaleY;
        result[10] = scaleZ;
        result[15] = 1;
        return result;
    }

    static public Matrix3D RotateX(float angle)
    {
        angle *= Mathf.Deg2Rad;

        Matrix3D result = new Matrix3D();
        result[0] = Mathf.Cos(angle);
        result[1] = -Mathf.Sin(angle);
        result[4] = Mathf.Sin(angle);
        result[5] = Mathf.Cos(angle);
        result[10] = 1;
        result[15] = 1;
        return result;
    }

    static public Matrix3D RotateY(float angle)
    {
        angle *= Mathf.Deg2Rad;

        Matrix3D result = new Matrix3D();
        result[0] = 1;
        result[5] = Mathf.Cos(angle);
        result[6] = -Mathf.Sin(angle);
        result[9] = Mathf.Sin(angle);
        result[10] = Mathf.Cos(angle);
        result[15] = 1;
        return result;

    }

    static public Matrix3D RotateZ(float angle)
    {
        angle *= Mathf.Deg2Rad;


        Matrix3D result = new Matrix3D();
        result[0] = Mathf.Cos(angle);
        result[2] = Mathf.Sin(angle);
        result[5] = 1;
        result[8] = -Mathf.Sin(angle);
        result[10] = Mathf.Cos(angle);
        result[15] = 1;
        return result;

    }



}
