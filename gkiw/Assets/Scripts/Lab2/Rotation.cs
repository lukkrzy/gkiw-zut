﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {


        Debug.Log("Przes * Obrot\n" + Matrix3D.Translate(-2, 0, 0) * Matrix3D.RotateX(90) * new Vector4(0, 0, 0, 1));
        Debug.Log("Obrot * Przes\n" + Matrix3D.RotateX(90) * Matrix3D.Translate(-2, 0, 0) * new Vector4(0, 0, 0, 1));

    }

    // Update is called once per frame
    void Update()
    {


        angle += speed * Time.deltaTime * 10;

        transform.position =
            (Vector3)(GetMatrix() *
            new Vector4(0, 0, 0, 1));
  
    }

    public Matrix3D GetMatrix() 
    {
        if (!center)
            return Matrix3D.Translate(transform.position.x, transform.position.y, transform.position.z);

        return center.GetMatrix() * center.GetRotationMatrix() * Matrix3D.Translate(r * Mathf.Cos(Mathf.Deg2Rad * angle) * scale, 0, r * Mathf.Sin(Mathf.Deg2Rad * angle));
    }

    public Matrix3D GetRotationMatrix()
    {
        return Matrix3D.RotateZ(transform.eulerAngles.z) * Matrix3D.RotateY(transform.eulerAngles.y) * Matrix3D.RotateX(transform.eulerAngles.x);
    }


    public Rotation center = null;
    public float r = 4.0f;
    public float speed;
    public float scale = 1.0f;

    private float angle = 0;

}