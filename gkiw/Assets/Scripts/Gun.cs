﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject bullet = null;
    public float shotPower = 500.0F;


    private ItemCollector itemCollector = null;
    // Use this for initialization
    void Start()
    {
        itemCollector = GetComponent<ItemCollector>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && itemCollector.isItem() && itemCollector.getItem().name =="AK-47") 
        {
            Debug.Log("Fire");
            GameObject obj = Instantiate(bullet);
            obj.transform.forward = transform.forward;
            obj.transform.position = transform.position + transform.forward + new Vector3(-0.3F, 1.5F, 0.0F);
            obj.SetActive(true);
            obj.GetComponent<Rigidbody>().AddForce(transform.forward * shotPower, ForceMode.Force);

        }
    }


}
