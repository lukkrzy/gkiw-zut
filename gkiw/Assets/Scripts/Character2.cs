﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2 : MonoBehaviour
{

    private Rigidbody rigidbody;
    public float speed = 10.0F;
    public float jumpSpeed = 10.0F;
    public float rotationSpeed = 100.0F;
    public float distToGround;

    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    private void Lean(int dir)
    {
        Vector3 leanAngle = transform.rotation.eulerAngles;
        if (dir == 1)
        {
            if (leanAngle.x < 30.0F || leanAngle.x > 330.0F)
                leanAngle.x += rotationSpeed * Time.deltaTime;

            if (leanAngle.x > 30.0F && leanAngle.x < 180.0F)
                leanAngle.x = 30.0F;
        }
            if (dir == 2)
            {
                if (leanAngle.x > 330.0F|| leanAngle.x <30.0F)
                    leanAngle.x -= rotationSpeed * Time.deltaTime;

                if (leanAngle.x < 330.0F && leanAngle.x>180.0F)
                    leanAngle.x = 330.0F;
            }
            if (dir==0)
            {
                if (leanAngle.x > 0.1F&& leanAngle.x < 180.0F)
                    leanAngle.x -= rotationSpeed * Time.deltaTime;
                if (leanAngle.x < 359.9F && leanAngle.x > 180.0F)
                    leanAngle.x += rotationSpeed * Time.deltaTime;
            }

       // Debug.Log("kat "+leanAngle.x.ToString());
        transform.localRotation = Quaternion.Euler(leanAngle);
    }

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        Collider collider = GetComponent<Collider>();
        distToGround = collider.bounds.extents.y;
    }

    void FixedUpdate()
    {
        if (IsGrounded())
        {
            //Debug.Log("is grounded");
            Vector3 moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));


            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

            moveDirection = transform.TransformDirection(moveDirection);
            rigidbody.AddForce(moveDirection * speed * Time.deltaTime, ForceMode.VelocityChange);


            Quaternion deltaRotation = Quaternion.Euler(0, Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed, 0);

            rigidbody.MoveRotation(rigidbody.rotation * deltaRotation);
            if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.X)) {
                if (Input.GetKey(KeyCode.Z))
                {
                    Lean(1);
                }
                if (Input.GetKey(KeyCode.X))
                {
                    Lean(2);
                }
            }
            else
                if (Mathf.RoundToInt(transform.rotation.eulerAngles.x) !=0)
                Lean(0);
        }
       // Debug.Log("kat int " + Mathf.RoundToInt(transform.rotation.eulerAngles.x).ToString());
    }

}
