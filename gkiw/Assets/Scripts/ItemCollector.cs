﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollector : MonoBehaviour
{

    private GameObject item = null;
    public GameObject itemSprite = null;

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
       // Debug.Log("Item: TriggeredEnter1: " + collider.gameObject.name + collider.gameObject.tag);


        if (item == null && collider.gameObject.tag == "Item")
        {
            item = collider.gameObject;
            CollectItem();
        }
        else if (item == collider.gameObject)
        {
            item = null;
        }

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt) && item != null)
        {
            DropItem();
        }
    }

    public void DropItem()
    {
        itemSprite.GetComponent<ChangeImage>().ChangeSprite(null);
        item.SetActive(true);
        item.transform.position = transform.position + transform.forward * 0.5F + transform.right * 0.5F;
        Debug.Log("item dropped! " + item.name);
    }

     void CollectItem()
    {
        Debug.Log("Podnosze item" + item.name);
        itemSprite.GetComponent<ChangeImage>().ChangeSprite(item);
        item.SetActive(false);
    }

    public void CollectItemFromFile(GameObject vItem)
    {
        item = vItem;
        Debug.Log("Podnosze item z pliku" + item.name);
        itemSprite.GetComponent<ChangeImage>().ChangeSprite(item);
        item.SetActive(false);
    }
    public bool isItem()
    {
        return item != null;
    }

    public GameObject getItem()
    {
        return item;
    }

    public void UpdateSprite()
    {
        if (!itemSprite)
            itemSprite = GameObject.Find("ImageHUD");
        Debug.Log("item to update: " + item);
        Debug.Log("itemSprite to update: " + itemSprite);
        itemSprite.GetComponent<ChangeImage>().ChangeSprite(item);

    }
}
