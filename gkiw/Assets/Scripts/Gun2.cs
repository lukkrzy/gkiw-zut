﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun2 : MonoBehaviour
{
    public GameObject bullet = null;
    public float shotPower = 50.0F;


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Fire");
            GameObject obj = Instantiate(bullet);
            obj.transform.forward = transform.forward;
            obj.transform.position = transform.position + transform.forward + new Vector3(-0.3F, 1.5F, 0.0F);
            obj.SetActive(true);
            obj.GetComponent<Rigidbody>().AddForce(transform.forward * shotPower, ForceMode.Force);

        }
    }


}
